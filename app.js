const express = require('express')
const mongoose = require('mongoose')
const authRoutes = require('./routes/auth')
const cookieParser = require('cookie-parser')
const { requireAuth, checkUser } = require('./middleware/auth')
const app = express()

// middleware
app.use(express.static('public'))
app.use(express.json({ urlencoded: true }))
app.use(cookieParser())

// view engine
app.set('view engine', 'ejs')

// database connection
const dbURI = 'mongodb+srv://msdcode:melasd2205@node-tutorial-netninja.oevnz.mongodb.net/node-jwtauth?retryWrites=true&w=majority'
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000))
  .catch((err) => console.log(err))

// routes
app.get('*', checkUser)
app.get('/', (req, res) => res.render('home'))
app.get('/smoothies', requireAuth, (req, res) => res.render('smoothies'))
app.use(authRoutes)
