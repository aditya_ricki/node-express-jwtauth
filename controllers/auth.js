const User = require('../models/User')
const jwt  = require('jsonwebtoken')

const handleErrors = (err) => {
	let errors = {
		email: '',
		password: ''
	}

	// login incorrect email
	if (err.message === 'incorrect email') {
		errors.email = 'This email is not registered'

		return errors
	}

	// login incorrect password
	if (err.message === 'incorrect password') {
		errors.password = 'This password is incorrect'

		return errors
	}

	// email already exists
	if (err.code === 11000) {
		errors.email = 'That email is already registered'

		return errors
	}

	// validation errors
	if (err.message.includes('user validation failed')) {
		Object.values(err.errors).forEach(({properties}) => {
			errors[properties.path] = properties.message
		})
	}

	return errors
}

// create token
// maxAge = second
const maxAge = 3 * 24 * 60 * 60
const createToken = (id) => {
	return jwt.sign({
		id
	}, 'melasdtrsyg', {
		expiresIn: maxAge
	})
}

module.exports.get_signup = (req, res) => {
	res.render('auth/signup')
}

module.exports.post_signup = async (req, res) => {
	const { email, password } = req.body

	try {
		const user = await User.create({ email, password })
		const token = createToken(user._id)

		res.cookie('jwt', token, {
			httpOnly: true,
			maxAge: maxAge * 1000
		})

		res.status(201).json({ user: user._id })
	}
	catch (err) {
		const errors = handleErrors(err)
		res.status(500).json({ errors })
	}
}

module.exports.get_login = (req, res) => {
	res.render('auth/login')
}

module.exports.post_login = async (req, res) => {
	const { email, password } = req.body

	try {
		const user = await User.login(email, password)
		const token = createToken(user._id)

		res.cookie('jwt', token, {
			httpOnly: true,
			maxAge: maxAge * 1000
		})
		res.status(200).json({ user: user._id })
	}
	catch (err) {
		const errors = handleErrors(err)
		res.status(500).json({ errors })
	}
}

module.exports.get_logout = (req, res) => {
	res.cookie('jwt', '', { maxAge: 1 })
	res.redirect('/')
}
