const User = require('../models/User')
const jwt = require('jsonwebtoken')

const requireAuth = (req, res, next) => {
	const token = req.cookies.jwt

	// cek json web token ada & valid
	if (token) {
		jwt.verify(token, 'melasdtrsyg', (err, decodeToken) => {
			if (err) {
				console.log(err.message)
				res.redirect('/login')
			} else {
				console.log(decodeToken)
				next()
			}
		})
	} else {
		res.redirect('/login')
	}
}

// cek current user
const checkUser = (req, res, next) => {
	const token = req.cookies.jwt

	// cek json web token ada & valid
	if (token) {
		jwt.verify(token, 'melasdtrsyg', async (err, decodeToken) => {
			if (err) {
				console.log(err.message)
				res.locals.user = null
				next()
			} else {
				console.log(decodeToken)
				let user = await User.findById(decodeToken.id)
				res.locals.user = user
				next()
			}
		})
	} else {
		res.locals.user = null
		next()
	}
}

module.exports = { requireAuth, checkUser }